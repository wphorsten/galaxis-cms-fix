
Meteor.publish('singlePost', function (id) {
	return Posts.find({_id: id}, {limit: 1});
});

Meteor.publish('latestPosts', function () {
	return Posts.find({}, {
		limit: 3,
		sort: {created_at: -1}
	});
})