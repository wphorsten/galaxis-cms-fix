
Meteor.methods({
	'publishPost': function (post) {
		console.log("publishing new post");
		if(Meteor.user()){
			console.log("user is logged in");
			if (Roles.userIsInRole(Meteor.user()._id, ['super-admin', 'admin', 'blogger'], Roles.GLOBAL_GROUP)) {
				//	User is logged in and has sufficient access.

				//	Convert tags to lowercase
				var lowerTags = [];
				for (var i = 0; i < post.tags.length; i++) {
				    lowerTags.push(post.tags[i].toLowerCase());
				}

				Posts.insert({
					title: post.title,
					content: post.content,
					summary: post.summary,
					author: {
						id: Meteor.user()._id,
						name: Meteor.users.find({_id: Meteor.user()._id}, {limit: 1}).fetch()[0].profile.name
					},
					featured_url: post.featured_url,
					featured_thumbnail: post.featured_thumbnail,
					created_at: new Date(),
					tags: lowerTags
				});
			}
		}
	},
	'editPost': function (post) {
		console.log("editing new post");
		if(Meteor.user()){
			console.log("user is logged in");
			if (Roles.userIsInRole(Meteor.user()._id, ['super-admin', 'admin', 'blogger'], Roles.GLOBAL_GROUP)) {
				//	User is logged in and has sufficient access.

				//	Convert tags to lowercase
				var lowerTags = [];
				for (var i = 0; i < post.tags.length; i++) {
				    lowerTags.push(post.tags[i].toLowerCase());
				}

				Posts.update(post._id, {
					$set:  {
						title: post.title,
						content: post.content,
						summary: post.summary,
						author: {
							id: Meteor.user()._id,
							name: Meteor.users.find({_id: Meteor.user()._id}, {limit: 1}).fetch()[0].profile.name
						},
						featured_url: post.featured_url,
						featured_thumbnail: post.featured_thumbnail,
						tags: lowerTags
					}
				});
			}
		}
	},
	'removePost': function (postId) {
		console.log("removing new post");
		if(Meteor.user()){
			console.log("user is logged in");
			if (Roles.userIsInRole(Meteor.user()._id, ['super-admin', 'admin', 'blogger'], Roles.GLOBAL_GROUP)) {
				//	User is logged in and has sufficient access.

				Posts.remove(postId);
			}
		}
	},
	'createRandomPosts': function () {
		for(var i = 0; i < 500; i++){
			Posts.insert({
				title: faker.commerce.productName(),
				content: faker.lorem.paragraphs(),
				summary: faker.lorem.paragraph(),
				author: {
					id: null,
					name: faker.name.firstName()
				},
				featured_url: faker.image.image(),
				featured_thumbnail: faker.image.image(),
				created_at: new Date(),
				tags: []
			});
		}
	}
});