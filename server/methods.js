/*****************************************************************************/
/*  Server Methods */
/*****************************************************************************/

Meteor.methods({
  'setupAdmin': function () {
  	//	Get amount of users
  	//	If there is only one user we make him super admin
  	var users = Meteor.users.find({}).fetch();
  	if(users.length === 1){
  		Roles.addUsersToRoles(users[0]._id, 'super-admin', Roles.GLOBAL_GROUP)
  	}
  }
});
