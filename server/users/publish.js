
//	Always publish own userdata
Meteor.publish(null, function() {
  	return Meteor.users.find({_id: this.userId}, {fields: {
  		'_id': 1,
		'services.google.picture': 1,
		'services.google.name': 1,
		'services.google.email': 1,
		'services.toggl.apikey': 1,
		'profile': 1
  	}});
});

Meteor.publish('allUsers', function() {
  	if(this.userId){
  		if (Roles.userIsInRole(this.userId, ['super-admin', 'admin', 'blogger'], Roles.GLOBAL_GROUP)) {
		  	return Meteor.users.find({});
  		}
  	}
});