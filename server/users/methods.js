
Meteor.methods({
	'enableBlogger': function (id) {
		if(Meteor.user()){
			if (Roles.userIsInRole(Meteor.user()._id, ['super-admin', 'admin'], Roles.GLOBAL_GROUP)) {
				//	Add this user to the blogger group
				Roles.addUsersToRoles(id, ['blogger'], Roles.GLOBAL_GROUP);
			}
		}
	},
	'disableBlogger': function (id) {
		if(Meteor.user()){
			if (Roles.userIsInRole(Meteor.user()._id, ['super-admin', 'admin'], Roles.GLOBAL_GROUP)) {
				//	Remove this user to the blogger group
				Roles.removeUsersFromRoles(id, ['blogger'], Roles.GLOBAL_GROUP);
			}
		}
	},
});