Accounts.onCreateUser(function(options, user) {

	if (user.services.google !== undefined) {
	    options.profile.picture = user.services.google.picture;
	}
	if (user.services.twitter !== undefined) {
	    options.profile.picture = user.services.twitter.profile_url;
	}
	if (user.services.facebook !== undefined) {
	    options.profile.picture = "http://graph.facebook.com/" + user.services.facebook.id + "/picture/?type=large";
	}

	user.profile = options.profile;

    return user;
});