Meteor.startup(function () {
 	process.env.MAIL_URL = 'smtp://postmaster%40sandboxf49ef1847dca4209bc47c3ff96ae4f68.mailgun.org:be114b92766d3a19ff68d102c63ff62d@smtp.mailgun.org:587';
});

// on the server, we create the sendEmail RPC function
Meteor.methods({
	sendEmail: function(emailOptions) {
		// send the email!
		// Email.send({
		// 	to: emailOptions.email,
		// 	from:'momics.eu@gmail.com',
		// 	subject:'Thank you for signing up for our project',
		// 	text:'We will share with you some news about us in a near future. See you soon!'
		// });

		var addition = "";
		if(emailOptions.wantsCookie){
			addition = "... Oh, and yes he'd like a cookie";
		}

		var emailData = {
			name: emailOptions.name,
			email: emailOptions.email,
			content: emailOptions.message,
			cookie: addition
		}

		SSR.compileTemplate( 'htmlEmail', Assets.getText('email_templates/ContactReceived.html' ) );
		var html = SSR.render( 'htmlEmail', emailData );

		// send the email!
		Email.send({
			from:'Momics <momics.eu@gmail.com>',
			to:'momics.eu@gmail.com',
			subject: emailOptions.name + ' filled a contact form',
			html: html
		});

		// Contacts.insert({
		// 	name: "name",
		// 	email: email
		// });
	}
});