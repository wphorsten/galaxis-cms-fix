# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Must have ###
* Mailgun
* Search box Reader (+ tag filter)
* Comments
* Media selector for new post


### Should have ###
* **Posts Page**
    * Tag Filter

### Could have ###
* Post statistics (visit time & amount of visits)
* Dashboard with graphics
   * User Activity
   * Post Activity
   * Read vs Unread Articles
* Related posts based on word analysis


### Finished Features ###
* Image Upload
* Edit Post (content, title, tags, date, creator, private/public)
* Tags for posts (&& || Categories)
* Search box Admin (+ tag filter)
* Social Media buttons