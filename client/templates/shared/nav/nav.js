/*****************************************************************************/
/* Nav: Event Handlers */
/*****************************************************************************/
Template.Nav.events({
});

/*****************************************************************************/
/* Nav: Helpers */
/*****************************************************************************/
Template.Nav.helpers({
    'isHomeRoute': function () {
        if( Router.current().route.getName() == 'home'){
            return true;
        }
        return false;
    }
});

/*****************************************************************************/
/* Nav: Lifecycle Hooks */
/*****************************************************************************/
Template.Nav.onCreated(function () {
});

Template.Nav.onRendered(function () {
    $(".button-collapse").sideNav({
        closeOnClick: true
    });

	$('.collapsible').collapsible({
      accordion : true
    });

    $(".button-collapse").sideNav();

    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        // Do something
        if(scroll > 400 && !$("#main-nav").hasClass("solid-nav")){
            $("#main-nav").addClass("solid-nav");
            $("#main-nav").removeClass("transparent-nav");
        } else if (scroll < 400) {
            $("#main-nav").removeClass("solid-nav");
            $("#main-nav").addClass("transparent-nav");
        }
    });
});

Template.Nav.onDestroyed(function () {
});
