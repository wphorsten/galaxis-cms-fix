/*****************************************************************************/
/* SearchBar: Event Handlers */
/*****************************************************************************/
Template.SearchBar.events({
    'keydown #search': function (e) {
        if(e.which == 13){
            e.preventDefault();

            //  Empty Search Bar
            $(e.currentTarget).val("");
        }
    },
    'keyup #search': function (e) {
        if(e.which !== 13){
            //Get search term
            var searchTerm = $(e.currentTarget).val();

            if(searchTerm == ""){
                Pages2.set({
                    filters: {}
                });
            }else{
                Pages2.set({
                    filters: {
                        $text: {$search: searchTerm},
                        //tags: searchTerm.toLowerCase()
                    }
                });
            }
        }
    },
});

/*****************************************************************************/
/* Nav: Helpers */
/*****************************************************************************/
Template.SearchBar.helpers({
});

/*****************************************************************************/
/* SearchBar: Lifecycle Hooks */
/*****************************************************************************/
Template.SearchBar.onCreated(function () {
});

Template.SearchBar.onRendered(function () {
});

Template.SearchBar.onDestroyed(function () {
});
