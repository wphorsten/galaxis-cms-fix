/*****************************************************************************/
/* FacebookLoginButton: Event Handlers */
/*****************************************************************************/
Template.FacebookLoginButton.events({
	'click #facebookButton': function (e) {
		e.preventDefault();
		Meteor.loginWithFacebook();
	}
});

/*****************************************************************************/
/* FacebookLoginButton: Helpers */
/*****************************************************************************/
Template.FacebookLoginButton.helpers({
});

/*****************************************************************************/
/* FacebookLoginButton: Lifecycle Hooks */
/*****************************************************************************/
Template.FacebookLoginButton.onCreated(function () {
});

Template.FacebookLoginButton.onRendered(function () {
});

Template.FacebookLoginButton.onDestroyed(function () {
});
