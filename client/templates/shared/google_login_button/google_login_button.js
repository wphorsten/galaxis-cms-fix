/*****************************************************************************/
/* GoogleLoginButton: Event Handlers */
/*****************************************************************************/
Template.GoogleLoginButton.events({
	'click #googleButton': function (e) {
		e.preventDefault();
		Meteor.loginWithGoogle();
	}
});

/*****************************************************************************/
/* GoogleLoginButton: Helpers */
/*****************************************************************************/
Template.GoogleLoginButton.helpers({
});

/*****************************************************************************/
/* GoogleLoginButton: Lifecycle Hooks */
/*****************************************************************************/
Template.GoogleLoginButton.onCreated(function () {
});

Template.GoogleLoginButton.onRendered(function () {
});

Template.GoogleLoginButton.onDestroyed(function () {
});
