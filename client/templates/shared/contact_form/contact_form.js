// on the client, we just have to track the DOM click event on the input.
Template.ContactForm.events({
    'submit #contactForm': function (e) {
    	e.preventDefault();
      // if someone click on the button ( tag), then we ask the server to execute the function sendEmail (RPC call)

      var emailOptions = {
      	name: $('#contactName').val(),
      	email: $('#contactEmail').val(),
      	message: $('#contactMessage').val(),
      	wantsCookie: $('#wantsCookie').is(':checked')
      }

      Meteor.call('sendEmail', emailOptions);
      Session.set('done', true);
    }
});

Template.ContactForm.helpers({
	done: function () {
		return Session.get('done');
	}
});