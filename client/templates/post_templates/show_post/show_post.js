/*****************************************************************************/
/* ShowPost: Event Handlers */
/*****************************************************************************/
Template.ShowPost.events({
});

/*****************************************************************************/
/* ShowPost: Helpers */
/*****************************************************************************/
Template.ShowPost.helpers({
	'getPost': function () {
		return Posts.findOne();
	},
	'shareData': function () {
		var thisPost = Posts.findOne(Router.current().params._id);
		return {
			title: thisPost.title,
			excerpt: thisPost.summary,
			description: thisPost.summary,
			summary: thisPost.summary,
			thumbnail: thisPost.featured_thumbnail,
			media: thisPost.featured_thumbnail
		};
	},
	'getTimeAgo': function (date) {
		var thisDate = moment(date);
		return thisDate.fromNow();
	}
});

/*****************************************************************************/
/* ShowPost: Lifecycle Hooks */
/*****************************************************************************/
Template.ShowPost.onCreated(function () {
});

Template.ShowPost.onRendered(function () {
	vph = $(window).height();
	if(vph < 400){
		vph = 400;
	}
    $('.parallax-container').height(vph - 250);

	$('.parallax').parallax();
});

Template.ShowPost.onDestroyed(function () {
});
