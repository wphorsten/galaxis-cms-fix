/*****************************************************************************/
/* PostListItem: Event Handlers */
/*****************************************************************************/
Template.PostListItem.events({
});

/*****************************************************************************/
/* PostListItem: Helpers */
/*****************************************************************************/
Template.PostListItem.helpers({
	'getSummary': function () {
		return this.summary;
	},
	'getTimeAgo': function (date) {
		var thisDate = moment(date);
		return thisDate.fromNow();
	}
});

/*****************************************************************************/
/* PostListItem: Lifecycle Hooks */
/*****************************************************************************/
Template.PostListItem.onCreated(function () {
});

Template.PostListItem.onRendered(function () {
});

Template.PostListItem.onDestroyed(function () {
});
