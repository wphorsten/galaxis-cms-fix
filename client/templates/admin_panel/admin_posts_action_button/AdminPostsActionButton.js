Session.set('showNewPost', false);

/*****************************************************************************/
/* AdminPostsActionButton: Event Handlers */
/*****************************************************************************/
Template.AdminPostsActionButton.events({
	'click #newPostButton': function () {
		console.log('kek');
		Session.set('showNewPost', true);
	},
	'click #closeNewPost': function () {
		Session.set('showNewPost', false);
	}
});

/*****************************************************************************/
/* AdminPostsActionButton: Helpers */
/*****************************************************************************/
Template.AdminPostsActionButton.helpers({
	'showNewPost': function () {
		return Session.get('showNewPost');
	}
});