/*****************************************************************************/
/* AdminQuickMenu: Event Handlers */
/*****************************************************************************/
Template.AdminQuickMenu.events({
});

/*****************************************************************************/
/* AdminQuickMenu: Helpers */
/*****************************************************************************/
Template.AdminQuickMenu.helpers({
	'getPostId': function () {
		return Posts.findOne()._id;
	}
});

/*****************************************************************************/
/* AdminQuickMenu: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminQuickMenu.onCreated(function () {
});

Template.AdminQuickMenu.onRendered(function () {
});

Template.AdminQuickMenu.onDestroyed(function () {
});
