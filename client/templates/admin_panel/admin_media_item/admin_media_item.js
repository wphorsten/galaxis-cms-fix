Template.AdminMediaItem.helpers({
	'getImageThumbnail': function (data) {
		var thisPicture = Media.findOne(data._id);
		return thisPicture.url({store: 'thumbs'});
	},
	'getImageFull': function (data) {
		var thisPicture = Media.findOne(data._id);
		return thisPicture.url({store: 'media'});
	}
});
