/*****************************************************************************/
/* AdminPostPreview: Event Handlers */
/*****************************************************************************/
Template.AdminPostPreview.events({
});

/*****************************************************************************/
/* AdminPostPreview: Helpers */
/*****************************************************************************/
Template.AdminPostPreview.helpers({
});

/*****************************************************************************/
/* AdminPostPreview: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminPostPreview.onCreated(function () {
});

Template.AdminPostPreview.onRendered(function () {
});

Template.AdminPostPreview.onDestroyed(function () {
});
