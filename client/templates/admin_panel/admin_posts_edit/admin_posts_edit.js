Session.set('showEditPost', false);	

/*****************************************************************************/
/* AdminPostsEdit: Event Handlers */
/*****************************************************************************/
Template.AdminPostsEdit.events({
	'click #closeEditPost': function () {
		Session.set('showEditPost', false);
	},
	'submit #editPost': function (e) {
		e.preventDefault();

		var thePost = {
			_id: Session.get('adminFocusedPost'),
			title: $('.admin-posts-edit #postTitle').val(), 
			summary: $('.admin-posts-edit #postSummary').val(), 
			content: $('.admin-posts-edit #postContent').val(),
			featured_url: $('.admin-posts-edit #featuredUrl').val(),
			featured_thumbnail: $('.admin-posts-edit #featuredUrl').val().replace("store=media", "store=thumbs"),
			tags: Session.get('currentPostTags')
		};

		Meteor.call('editPost', thePost, function (error, result) {
			if(!error){
				Session.set('showEditPost', false);
				Materialize.toast('Post succesfully edited!', 1000);
			}else{
				Materialize.toast('<span class="red-text text-lighten-2">Darn, we weren\'t able to edit this post!</span>', 1000);
			}
			
		});
	},
	'keyup #postContent': function (e){
		Session.set('currentMarkdownText', $('.admin-posts-edit #postContent').val());
	},
	'keyup #featuredUrl': function (e){
		Session.set('currentPostFeatured', $('.admin-posts-edit #featuredUrl').val());
	},
	'keyup #postTitle': function (e){
		Session.set('currentPostTitle', $('.admin-posts-edit #postTitle').val());
	},
	'keyup #postSummary': function (e){
		Session.set('currentPostSummary', $('.admin-posts-edit #postSummary').val());
	},
	'keydown #postTagInput': function (e){
		if(e.which == 13){
			e.preventDefault();
			var allTags = Session.get('currentPostTags');
			allTags.push($('.admin-posts-edit #postTagInput').val());
			Session.set('currentPostTags', allTags);
			console.log('Added ' + $('.admin-posts-edit #postTagInput').val());
			$('.admin-posts-edit #postTagInput').val("");
		}
	},
	'click .tag-remover': function (e) {
		e.preventDefault();
		var currentTags = Session.get('currentPostTags');
		var thisTag = $(e.currentTarget).clone()    //clone the element
		    .children() //select all the children
		    .remove()   //remove all the children
		    .end()  //again go back to selected element
		    .text();

		//	Remove this one from the tags
		var index = currentTags.indexOf(thisTag);
		if (index > -1) {
		    currentTags.splice(index, 1);
		}
		Session.set('currentPostTags', currentTags);
	},
	'click #night-mode': function (e) {
		var black = "#242424";
		var white = "#E8E8E8";

		if($(".admin-posts-edit #night-mode").attr('data-mode') == "day"){
			$('.admin-posts-edit').css('background-color', black);
			$('.markup-menu').css('background-color', black);


			$('.post-container').children().css('color', white);
			$('.admin-posts-edit').css('color', white);

			$(".admin-posts-edit #night-mode").attr('data-mode', "night");
			$(".admin-posts-edit #night-mode .material-icons").html("brightness_low");
		}else{
			$('.admin-posts-edit').css('background-color', '#fff');
			$('.markup-menu').css('background-color', '#fff');

			$('.post-container').children().css('color', '#000');
			$('.admin-posts-edit').css('color', '#000');

			$(".admin-posts-edit #night-mode").attr('data-mode', "day");
			$(".admin-posts-edit #night-mode .material-icons").html("brightness_high");
		}
	},
	'change #mediaInput': function () {
		var files = event.target.files;
	    for (var i = 0, ln = files.length; i < ln; i++) {
	      Media.insert(files[i], function (err, fileObj) {
	        // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
	        if(!err){
	        	Materialize.toast('Successfully uploaded media.', 1000);
	        	insertAtCaret('postContent', '!['+fileObj.original.name+'](/cfs/files/media/' + fileObj._id + '/media?store=media)');
	        	Session.set('currentMarkdownText', $('.admin-posts-edit #postContent').val());
	        }else{
	        	console.log(err);
	        	Materialize.toast('Something went wrong.', 1000);
	        }
	      });
	    }
	},
	'change #featuredInput': function () {
		var files = event.target.files;
	    for (var i = 0, ln = files.length; i < ln; i++) {
	      Media.insert(files[i], function (err, fileObj) {
	        // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
	        if(!err){
	        	Materialize.toast('Successfully uploaded media.', 1000);
	        	$('.admin-posts-edit #featuredUrl').val('/cfs/files/media/' + fileObj._id + '/media?store=media');
	        }else{
	        	console.log(err);
	        	Materialize.toast('Something went wrong.', 1000);
	        }
	      });
	    }
	},
	'click #makeBold': function (e) {
		insertAndWrapCursor('postContent', '**', '**');
		Session.set('currentMarkdownText', $('.admin-posts-edit #postContent').val());
	},
	'click #makeItalic': function (e) {
		insertAndWrapCursor('postContent', '*', '*');
		Session.set('currentMarkdownText', $('.admin-posts-edit #postContent').val());
	},
	'click #makeQuote': function (e) {
		insertAndWrapCursor('postContent', '>', '');
		Session.set('currentMarkdownText', $('.admin-posts-edit #postContent').val());
	},
	'click #makeCaption': function (e) {
		insertAndWrapCursorFocus('postContent', '<p class="caption">', '</p>', 19);
		Session.set('currentMarkdownText', $('.admin-posts-edit #postContent').val());
	},
	'click #insertLink': function (e) {
		insertAndWrapCursorFocus('postContent', "[](", ")", 1);
		Session.set('currentMarkdownText', $('.admin-posts-edit #postContent').val());
	},
	'click #markup-edit': function (e){
		$(".admin-posts-edit #editButton").trigger('click');
	}
});

/*****************************************************************************/
/* AdminPostsEdit: Helpers */
/*****************************************************************************/
Template.AdminPostsEdit.helpers({
	'getMarkdownText': function () {
		return Session.get('currentMarkdownText');
	},
	'getPostTitle': function () {
		return Session.get('currentPostTitle');
	},
	'getPostFeatured': function () {
		return Session.get('currentPostFeatured');
	},
	'getPostSummary': function () {
		return Session.get('currentPostSummary');
	},
	'getOldFeatured': function () {		
		return Posts.findOne(Session.get('adminFocusedPost')).featured_url;
	},
	'getOldSummary': function () {		
		return Posts.findOne(Session.get('adminFocusedPost')).summary;
	},
	'getOldTitle': function () {	
		return Posts.findOne(Session.get('adminFocusedPost')).title;
	},
	'getOldContent': function () {
		return Posts.findOne(Session.get('adminFocusedPost')).content;
	},
	'showMe': function () {
		return Session.get('showEditPost');
	},
	'getTags': function () {
		return Session.get('currentPostTags');
	}
});

/*****************************************************************************/
/* AdminPostsEdit: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminPostsEdit.onCreated(function () {
	this.autorun(function () {
		if(Session.get('showEditPost')){
			triggerAllInputs();
		}

		//	Makes sure we load the tags once the post is ready.
		var thisPost = Posts.findOne(Session.get('adminFocusedPost'));
		if(thisPost){
			if(Object.prototype.toString.call(thisPost.tags) === '[object Array]' ){
				Session.set('currentPostTags', thisPost.tags);
			}else {
				Session.set('currentPostTags', []);
			}
		}
	});
});

Template.AdminPostsEdit.onRendered(function () {

	$('.dropdown-button').dropdown({
	      inDuration: 300,
	      outDuration: 225,
	      constrain_width: false, // Does not change width of dropdown to that of the activator
	      hover: true, // Activate on hover
	      gutter: 0, // Spacing from edge
	      belowOrigin: false, // Displays dropdown below the button
	      alignment: 'left' // Displays dropdown with edge aligned to the left of button
	});

	triggerAllInputs();

	Session.set('currentMarkdownText', "");
	Session.set('currentPostTitle', "");
	Session.set('currentPostFeatured', "");
	Session.set('currentPostSummary', "");
	
});

Template.AdminPostsEdit.onDestroyed(function () {
});



function triggerAllInputs () {
	setTimeout(function () {
		var e = jQuery.Event("keyup", {keyCode: 40});

		$(".admin-posts-edit #postTitle").focus();
		$(".admin-posts-edit #postTitle").trigger(e);
	}, 400);

	setTimeout(function () {
		var e = jQuery.Event("keyup", {keyCode: 40});

		$(".admin-posts-edit #postSummary").focus();
		$(".admin-posts-edit #postSummary").trigger(e);		
	}, 450);

	setTimeout(function () {
		var e = jQuery.Event("keyup", {keyCode: 40});

		$(".admin-posts-edit #featuredUrl").focus();
		$(".admin-posts-edit #featuredUrl").trigger(e);		
	}, 500);

	setTimeout(function () {
		var e = jQuery.Event("keyup", {keyCode: 40});

		$(".admin-posts-edit #postContent").focus();
		$(".admin-posts-edit #postContent").trigger(e);
	}, 550);
}

function insertAndWrapCursorFocus(areaId, beforeVal, afterVal, focusDiff) {
	var myField = document.getElementById(areaId);
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = beforeVal + sel.text + afterVal;
    }
    //MOZILLA and others
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
            + beforeVal + myField.value.substring(startPos, endPos) + afterVal
            + myField.value.substring(endPos, myField.value.length);

        myField.setSelectionRange(startPos+focusDiff, startPos+focusDiff);
        myField.focus();
    } else {
        myField.value += "Please use a legitimate browser";
    }
}

function insertAndWrapCursor(areaId, beforeVal, afterVal) {
	var myField = document.getElementById(areaId);
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = beforeVal + sel.text + afterVal;
    }
    //MOZILLA and others
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
            + beforeVal + myField.value.substring(startPos, endPos) + afterVal
            + myField.value.substring(endPos, myField.value.length);
    } else {
        myField.value += "Please use a legitimate browser";
    }
}

function insertAtCursor(areaId, myValue) {
	var myField = document.getElementById(areaId);
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = myValue;
    }
    //MOZILLA and others
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
            + myValue
            + myField.value.substring(endPos, myField.value.length);
    } else {
        myField.value += myValue;
    }
}


function insertAtCaret(areaId,text) {
    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
        "ff" : (document.selection ? "ie" : false ) );
    if (br == "ie") { 
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        strPos = range.text.length;
    }
    else if (br == "ff") strPos = txtarea.selectionStart;

    var front = (txtarea.value).substring(0,strPos);  
    var back = (txtarea.value).substring(strPos,txtarea.value.length); 
    txtarea.value=front+text+back;
    strPos = strPos + text.length;
    if (br == "ie") { 
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        range.moveStart ('character', strPos);
        range.moveEnd ('character', 0);
        range.select();
    }
    else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }
    txtarea.scrollTop = scrollPos;
}