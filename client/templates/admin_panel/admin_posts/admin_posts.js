/*****************************************************************************/
/* AdminPosts: Event Handlers */
/*****************************************************************************/
Template.AdminPosts.events({
	'click #createDummyPosts': function (e){
		Meteor.call('createRandomPosts');
	}
});

/*****************************************************************************/
/* AdminPosts: Helpers */
/*****************************************************************************/
Template.AdminPosts.helpers({
	'showEditPost': function () {
		return Session.get('showEditPost');
	},
});

/*****************************************************************************/
/* AdminPosts: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminPosts.onCreated(function () {
});

Template.AdminPosts.onRendered(function () {
});

Template.AdminPosts.onDestroyed(function () {
});
