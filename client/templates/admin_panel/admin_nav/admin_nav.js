/*****************************************************************************/
/* AdminNav: Event Handlers */
/*****************************************************************************/
Template.AdminNav.events({
});

/*****************************************************************************/
/* AdminNav: Helpers */
/*****************************************************************************/
Template.AdminNav.helpers({
	'getUserAvatar': function () {
		return Meteor.user().profile.picture;
	},
	'getUsername': function () {
		return Meteor.user().profile.name;
	},
	'getRouteTitle': function () {
		return Router.current().route.options.title;
	}
});

/*****************************************************************************/
/* AdminNav: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminNav.onCreated(function () {
});

Template.AdminNav.onRendered(function () {
	$('.collapsible').collapsible({
      accordion : true
    });

    $(".button-collapse").sideNav();
});

Template.AdminNav.onDestroyed(function () {
});
