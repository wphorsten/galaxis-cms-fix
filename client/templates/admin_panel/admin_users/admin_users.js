/*****************************************************************************/
/* AdminUsers: Event Handlers */
/*****************************************************************************/
Template.AdminUsers.events({
	'click #enableBlogger': function (e) {
		e.preventDefault();

		Meteor.call('enableBlogger', $(e.currentTarget).attr('data-id'), function (error, result) {
			if(!error){
				Materialize.toast('Enabled this user as blogger!', 1000);
			}
		});
	},
	'click #disableBlogger': function (e) {
		e.preventDefault();

		Meteor.call('disableBlogger', $(e.currentTarget).attr('data-id'), function (error, result) {
			if(!error){
				Materialize.toast('Disabled this user as blogger!', 1000);
			}
		});
	},
});

/*****************************************************************************/
/* AdminUsers: Helpers */
/*****************************************************************************/
Template.AdminUsers.helpers({
	'getUsers': function () {
		return Meteor.users.find().fetch();
	}
});

/*****************************************************************************/
/* AdminUsers: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminUsers.onCreated(function () {
});

Template.AdminUsers.onRendered(function () {
});

Template.AdminUsers.onDestroyed(function () {
});
