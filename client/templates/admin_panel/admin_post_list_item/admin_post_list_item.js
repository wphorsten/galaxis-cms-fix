/*****************************************************************************/
/* AdminPostListItem: Event Handlers */
/*****************************************************************************/
Template.AdminPostListItem.events({
	'click .admin-post-list-item': function (e) {
		if($(e.target).prop("tagName") == "TD" || $(e.target).prop("tagName") == "TR"){
			e.preventDefault();

			Session.set('adminFocusedPost', $(e.currentTarget).attr('data-id'));
			Session.set('showEditPost', true);
		}
	},
	'click .remove-post': function (e){
		e.preventDefault();
		var postId = $(e.currentTarget).attr('data-id');

		Meteor.call('removePost', postId, function (error, result) {
			if(!error){
				Materialize.toast("Succesfully removed post", 1000);
			}else{
				Materialize.toast("Something went wrong", 1000);
			}
		});
	}
});

/*****************************************************************************/
/* AdminPostListItem: Helpers */
/*****************************************************************************/
Template.AdminPostListItem.helpers({
	'getDateString': function (date) {
		var thisMoment = moment(date);
		return thisMoment.format("HH:mm D-MM-YYYY");
	}
});

/*****************************************************************************/
/* AdminPostListItem: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminPostListItem.onCreated(function () {
});

Template.AdminPostListItem.onRendered(function () {
});

Template.AdminPostListItem.onDestroyed(function () {
});
