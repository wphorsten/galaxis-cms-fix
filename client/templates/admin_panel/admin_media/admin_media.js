/*****************************************************************************/
/* AdminMedia: Event Handlers */
/*****************************************************************************/
Template.AdminMedia.events({
	'change #mediaInput': function () {
		var files = event.target.files;
	    for (var i = 0, ln = files.length; i < ln; i++) {
	      Media.insert(files[i], function (err, fileObj) {
	        // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
	        if(!err){
	        	Materialize.toast('Successfully uploaded media.', 1000);
	        	console.log(fileObj);
	        	console.log(fileObj.url());
	        }else{
	        	console.log(err);
	        	Materialize.toast('Something went wrong.', 1000);
	        }
	      });
	    }
	}
});

/*****************************************************************************/
/* AdminMedia: Helpers */
/*****************************************************************************/
Template.AdminMedia.helpers({
});

/*****************************************************************************/
/* AdminMedia: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminMedia.onCreated(function () {
});

Template.AdminMedia.onRendered(function () {
});

Template.AdminMedia.onDestroyed(function () {
});
