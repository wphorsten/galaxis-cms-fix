/*****************************************************************************/
/* AdminPostList: Event Handlers */
/*****************************************************************************/
Template.AdminPostList.events({
});

/*****************************************************************************/
/* AdminPostList: Helpers */
/*****************************************************************************/
Template.AdminPostList.helpers({
});

/*****************************************************************************/
/* AdminPostList: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminPostList.onCreated(function () {
});

Template.AdminPostList.onRendered(function () {
});

Template.AdminPostList.onDestroyed(function () {
});
