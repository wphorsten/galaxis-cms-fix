Session.set('currentMarkdownText', "");
Session.set('currentPostTitle', "");
Session.set('currentPostFeatured', "");
Session.set('currentPostSummary', "");
Session.set('currentPostTags', []);

/*****************************************************************************/
/* AdminPostsNew: Event Handlers */
/*****************************************************************************/
Template.AdminPostsNew.events({
	'submit #newPost': function (e) {
		e.preventDefault();

		var thePost = {
			title: $('.admin-posts-new #postTitle').val(), 
			summary: $('.admin-posts-new #postSummary').val(), 
			content: $('.admin-posts-new #postContent').val(),
			featured_url: $('.admin-posts-new #featuredUrl').val(),
			featured_thumbnail: $('.admin-posts-new #featuredUrl').val().replace("store=media", "store=thumbs"),
			tags: Session.get('currentPostTags')
		};

		Meteor.call('publishPost', thePost, function (error, result) {
			if(!error){
				Session.set('showNewPost', false);
				Materialize.toast('Post succesfully published!', 1000);
			}else{
				Materialize.toast('<span class="red-text text-lighten-2">Darn, we weren\'t able to publish this post!</span>', 1000);
			}
			
		});
	},
	'keyup #postContent': function (e){
		Session.set('currentMarkdownText', $('.admin-posts-new #postContent').val());
	},
	'keyup #featuredUrl': function (e){
		Session.set('currentPostFeatured', $('.admin-posts-new #featuredUrl').val());
	},
	'keyup #postTitle': function (e){
		Session.set('currentPostTitle', $('.admin-posts-new #postTitle').val());
	},
	'keyup #postSummary': function (e){
		Session.set('currentPostSummary', $('.admin-posts-new #postSummary').val());
	},
	'keydown #postTagInput': function (e){
		if(e.which == 13){
			e.preventDefault();
			var allTags = Session.get('currentPostTags');
			allTags.push($('.admin-posts-new #postTagInput').val());
			Session.set('currentPostTags', allTags);

			$('.admin-posts-new #postTagInput').val("");
		}
	},
	'click .tag-remover': function (e) {
		e.preventDefault();
		var currentTags = Session.get('currentPostTags');
		var thisTag = $(e.currentTarget).clone()    //clone the element
		    .children() //select all the children
		    .remove()   //remove all the children
		    .end()  //again go back to selected element
		    .text();

		//	Remove this one from the tags
		var index = currentTags.indexOf(thisTag);
		if (index > -1) {
		    currentTags.splice(index, 1);
		}
		Session.set('currentPostTags', currentTags);
	},
	'click #night-mode': function (e) {
		var black = "#242424";
		var white = "#E8E8E8";

		if($(".admin-posts-new #night-mode").attr('data-mode') == "day"){
			$('.admin-posts-new').css('background-color', black);
			$('.markup-menu').css('background-color', black);


			$('.post-container').children().css('color', white);
			$('.admin-posts-new').css('color', white);

			$(".admin-posts-new #night-mode").attr('data-mode', "night");
			$(".admin-posts-new #night-mode .material-icons").html("brightness_low");
		}else{
			$('.admin-posts-new').css('background-color', '#fff');
			$('.markup-menu').css('background-color', '#fff');

			$('.post-container').children().css('color', '#000');
			$('.admin-posts-new').css('color', '#000');

			$(".admin-posts-new #night-mode").attr('data-mode', "day");
			$(".admin-posts-new #night-mode .material-icons").html("brightness_high");
		}
	},
	'change #mediaInput': function () {
		var files = event.target.files;
	    for (var i = 0, ln = files.length; i < ln; i++) {
	      Media.insert(files[i], function (err, fileObj) {
	        // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
	        if(!err){
	        	Materialize.toast('Successfully uploaded media.', 1000);
	        	insertAtCaret('postContent', '!['+fileObj.original.name+'](/cfs/files/media/' + fileObj._id + '/media?store=media)');
	        	Session.set('currentMarkdownText', $('.admin-posts-new #postContent').val());
	        }else{
	        	console.log(err);
	        	Materialize.toast('Something went wrong.', 1000);
	        }
	      });
	    }
	},
	'change #featuredInput': function () {
		var files = event.target.files;
	    for (var i = 0, ln = files.length; i < ln; i++) {
	      Media.insert(files[i], function (err, fileObj) {
	        // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
	        if(!err){
	        	Materialize.toast('Successfully uploaded media.', 1000);
	        	$('.admin-posts-new #featuredUrl').val('/cfs/files/media/' + fileObj._id + '/media?store=media');
	        }else{
	        	console.log(err);
	        	Materialize.toast('Something went wrong.', 1000);
	        }
	      });
	    }
	},
	'click #makeBold': function (e) {
		insertAndWrapCursor('postContent', '**', '**');
		Session.set('currentMarkdownText', $('.admin-posts-new #postContent').val());
	},
	'click #makeItalic': function (e) {
		insertAndWrapCursor('postContent', '*', '*');
		Session.set('currentMarkdownText', $('.admin-posts-new #postContent').val());
	},
	'click #makeQuote': function (e) {
		insertAndWrapCursor('postContent', '>', '');
		Session.set('currentMarkdownText', $('.admin-posts-new #postContent').val());
	},
	'click #makeCaption': function (e) {
		insertAndWrapCursorFocus('postContent', '<p class="caption">', '</p>', 19);
		Session.set('currentMarkdownText', $('.admin-posts-new #postContent').val());
	},
	'click #insertLink': function (e) {
		insertAndWrapCursorFocus('postContent', "[](", ")", 1);
		Session.set('currentMarkdownText', $('.admin-posts-new #postContent').val());
	},
	'click #markup-publish': function (e){
		$("#publishButton").trigger('click');
	}
});

/*****************************************************************************/
/* AdminPostsNew: Helpers */
/*****************************************************************************/
Template.AdminPostsNew.helpers({
	'getMarkdownText': function () {
		return Session.get('currentMarkdownText');
	},
	'getPostTitle': function () {
		return Session.get('currentPostTitle');
	},
	'getPostFeatured': function () {
		return Session.get('currentPostFeatured');
	},
	'getTags': function () {
		return Session.get('currentPostTags');
	},
	'getPostSummary': function () {
		return Session.get('currentPostSummary');
	}
});

/*****************************************************************************/
/* AdminPostsNew: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminPostsNew.onCreated(function () {
});

Template.AdminPostsNew.onRendered(function () {
	Session.set('currentMarkdownText', "");
	Session.set('currentPostTitle', "");
	Session.set('currentPostFeatured', "");
	Session.set('currentPostSummary', "");
	Session.set('currentPostTags', []);

	$('.admin-posts-new .dropdown-button').dropdown({
	      inDuration: 300,
	      outDuration: 225,
	      constrain_width: false, // Does not change width of dropdown to that of the activator
	      hover: true, // Activate on hover
	      gutter: 0, // Spacing from edge
	      belowOrigin: false, // Displays dropdown below the button
	      alignment: 'left' // Displays dropdown with edge aligned to the left of button
	});
});

Template.AdminPostsNew.onDestroyed(function () {
});




function insertAndWrapCursorFocus(areaId, beforeVal, afterVal, focusDiff) {
	var myField = document.getElementById(areaId);
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = beforeVal + sel.text + afterVal;
    }
    //MOZILLA and others
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
            + beforeVal + myField.value.substring(startPos, endPos) + afterVal
            + myField.value.substring(endPos, myField.value.length);

        myField.setSelectionRange(startPos+focusDiff, startPos+focusDiff);
        myField.focus();
    } else {
        myField.value += "Please use a legitimate browser";
    }
}

function insertAndWrapCursor(areaId, beforeVal, afterVal) {
	var myField = document.getElementById(areaId);
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = beforeVal + sel.text + afterVal;
    }
    //MOZILLA and others
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
            + beforeVal + myField.value.substring(startPos, endPos) + afterVal
            + myField.value.substring(endPos, myField.value.length);
    } else {
        myField.value += "Please use a legitimate browser";
    }
}

function insertAtCursor(areaId, myValue) {
	var myField = document.getElementById(areaId);
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = myValue;
    }
    //MOZILLA and others
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
            + myValue
            + myField.value.substring(endPos, myField.value.length);
    } else {
        myField.value += myValue;
    }
}


function insertAtCaret(areaId,text) {
    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
        "ff" : (document.selection ? "ie" : false ) );
    if (br == "ie") { 
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        strPos = range.text.length;
    }
    else if (br == "ff") strPos = txtarea.selectionStart;

    var front = (txtarea.value).substring(0,strPos);  
    var back = (txtarea.value).substring(strPos,txtarea.value.length); 
    txtarea.value=front+text+back;
    strPos = strPos + text.length;
    if (br == "ie") { 
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        range.moveStart ('character', strPos);
        range.moveEnd ('character', 0);
        range.select();
    }
    else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }
    txtarea.scrollTop = scrollPos;
}