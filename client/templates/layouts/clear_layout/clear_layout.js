/*****************************************************************************/
/* ClearLayout: Event Handlers */
/*****************************************************************************/
Template.ClearLayout.events({
});

/*****************************************************************************/
/* ClearLayout: Helpers */
/*****************************************************************************/
Template.ClearLayout.helpers({
});

/*****************************************************************************/
/* ClearLayout: Lifecycle Hooks */
/*****************************************************************************/
Template.ClearLayout.onCreated(function () {
});

Template.ClearLayout.onRendered(function () {
});

Template.ClearLayout.onDestroyed(function () {
});
