/*****************************************************************************/
/* AdminLayout: Event Handlers */
/*****************************************************************************/
Template.AdminLayout.events({
});

/*****************************************************************************/
/* AdminLayout: Helpers */
/*****************************************************************************/
Template.AdminLayout.helpers({
});

/*****************************************************************************/
/* AdminLayout: Lifecycle Hooks */
/*****************************************************************************/
Template.AdminLayout.onCreated(function () {
});

Template.AdminLayout.onRendered(function () {
});

Template.AdminLayout.onDestroyed(function () {
});
