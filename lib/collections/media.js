var createThumb = function(fileObj, readStream, writeStream) {
  // Transform the image into a 10x10px thumbnail
  gm(readStream, fileObj.name()).resize('150', '150').stream().pipe(writeStream);
};

var checkHD = function(fileObj, readStream, writeStream) {
  // Transform the image into a 10x10px thumbnail
  gm(readStream, fileObj.name()).resize('1920', '1080', '>').stream().pipe(writeStream);
};

Media = new FS.Collection("media", {
  stores: [
    new FS.Store.FileSystem("thumbs", { transformWrite: createThumb, path: "~/Desktop/uploads/thumbs"}),
    new FS.Store.FileSystem("media", { transformWrite: checkHD, path: "~/Desktop/uploads/media"}),
  ],
  filter: {
    maxSize: 2500000,
    allow: {
      contentTypes: ['image/*'] //allow only images in this FS.Collection
    }
  }
});


if (Meteor.isServer) {
  Media.allow({
    insert: function (userId, doc) {
      if (Roles.userIsInRole(userId, ['super-admin', 'admin', 'blogger'], Roles.GLOBAL_GROUP)) {
        return true;
      }
      return false;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return false;
    },

    remove: function (userId, doc) {
      return false;
    },

    download: function(userId, fileObj) {
      return true;
    }
  });

  Media.deny({
    insert: function (userId, doc) {
      if (Roles.userIsInRole(userId, ['super-admin', 'admin', 'blogger'], Roles.GLOBAL_GROUP)) {
        return false;
      }
      return true;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function (userId, doc) {
      return true;
    },

    download: function(userId, fileObj) {
        return false;
    }
  });
}