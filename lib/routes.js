Router.configure({
  layoutTemplate: 'ClearLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});

Router.onBeforeAction(function () {
  Pages.unsubscribe();
  Pages2.unsubscribe();
  Pages3.unsubscribe();
  this.next();
});

Router.route('/', {
  name: 'home',
  controller: 'HomeController',
  layoutTemplate: 'MasterLayout',
  where: 'client'
});






Router.route('logout', {
  name: 'logout',
  action: function () {
    if(Meteor.userId()){
      Meteor.logout();
    }
    this.layout('ClearLayout');
    Router.go('login');
  }
});

// Router.route('login', {
//   name: 'login',
//   where: 'client',
//   action: function () {
//     this.layout('ClearLayout');
//     this.render('Login');
//   }
// });

Router.route('show_post/:_id', {
  name: 'showPost',
  controller: 'ShowPostController',
  layoutTemplate: 'MasterLayout',
  where: 'client'
});



// ░█▀▀█ ▒█▀▀▄ ▒█▀▄▀█ ▀█▀ ▒█▄░▒█ 
// ▒█▄▄█ ▒█░▒█ ▒█▒█▒█ ▒█░ ▒█▒█▒█ 
// ▒█░▒█ ▒█▄▄▀ ▒█░░▒█ ▄█▄ ▒█░░▀█ 

Router.route('adminsetup', {
  name: 'adminSetup',
  action: function () {
    Meteor.call('setupAdmin', function (result, error) {
      if(!error){
        Router.go('/admin');
      }
    });
  },
  where: 'client'
});

Router.route('admin', {
  name: 'admin',
  title: 'Admin Panel',
  layoutTemplate: 'AdminLayout',
  controller: 'AdminController',
  where: 'client'
});

Router.route('admin/posts', {
  name: 'adminPosts',
  title: 'Posts',
  layoutTemplate: 'AdminLayout',
  controller: 'AdminPostsController',
  where: 'client'
});

Router.route('admin/posts/new', {
  name: 'adminPostsNew',
  title: 'New Post',
  layoutTemplate: 'AdminLayout',
  controller: 'AdminPostsNewController',
  where: 'client'
});

Router.route('admin/posts/edit/:_id', {
  name: 'adminPostsEdit',
  title: 'Edit Post',
  layoutTemplate: 'AdminLayout',
  controller: 'AdminPostsEditController',
  where: 'client'
});

Router.route('/admin/users', {
  name: 'adminUsers',
  title: 'Users',
  layoutTemplate: 'AdminLayout',
  controller: 'AdminUsersController',
  where: 'client'
});

Router.route('/admin/media', {
  name: 'adminMedia',
  title: 'Media',
  layoutTemplate: 'AdminLayout',
  controller: 'AdminMediaController',
  where: 'client'
});