
//  This creates a route at /posts
//  The Posts template is used as main route template
//  The PostListItem is used as post preview
//  Infinite scrolling is already enables.
this.Pages = new Meteor.Pagination(Posts, {
    availableSettings: {
      limit: true,
      sort: true,
      filters: true
    },
    infinite: true,
    infiniteTrigger: .9,
    infiniteRateLimit: 1,
    infiniteStep: 1,
    
    pageSizeLimit: 1000,
    perPage: 10,
    maxSubscriptions: 500,
    dataMargin: 1,

    router: "iron-router",

    //  Route path to be used for this route
    homeRoute: "/posts/",
    route: "/posts/",

    //  Router template to be used
    routerTemplate: "Posts",
    templateName: "Posts",

    //  Layout to be used for the above Router Template
    routerLayout: "MasterLayout",

    //  Post preview item template
    itemTemplate: "PostListItem",

    sort: {
      created_at: -1
    }
});



// ░█▀▀█ ▒█▀▀▄ ▒█▀▄▀█ ▀█▀ ▒█▄░▒█ 
// ▒█▄▄█ ▒█░▒█ ▒█▒█▒█ ▒█░ ▒█▒█▒█ 
// ▒█░▒█ ▒█▄▄▀ ▒█░░▒█ ▄█▄ ▒█░░▀█ 

this.Pages2 = new Meteor.Pagination(Posts, {
    sort: {
      created_at: -1
    },
    templateName: "AdminPostList",
    dataMargin: 0,
    itemTemplate: "AdminPostListItem",
    divWrapper: false,

    availableSettings: {
      limit: true,
      sort: true,
      filters: true
    },
    infinite: true,
    infiniteTrigger: .9,
    infiniteRateLimit: 1,
    infiniteStep: 1,
    
    pageSizeLimit: 1000,
    perPage: 10,
    maxSubscriptions: 500,
    dataMargin: 30,
});

this.Pages3 = new Meteor.Pagination(Media, {
    sort: {
      uploadedAt: -1
    },
    templateName: "AdminMediaList",
    itemTemplate: "AdminMediaItem",
    divWrapper: false,

    availableSettings: {
      limit: true,
      sort: true
    },
    infinite: true,
    infiniteTrigger: .9,
    infiniteRateLimit: 1,
    infiniteStep: 1,
    
    pageSizeLimit: 1000,
    perPage: 10,
    maxSubscriptions: 500,
    dataMargin: 30,
});