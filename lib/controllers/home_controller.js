HomeController = RouteController.extend({
  
  //  We subscribe for the featured posts.
  subscriptions: function() {
    this.subscribe('latestPosts');
  },
  
  onRun: function () {
    this.next();
  },
  onRerun: function () {
    this.next();
  },
  onBeforeAction: function () {
    this.next();
  },  
  action: function () {
    this.render();
  }
});
