ShowPostController = RouteController.extend({
  
  waitOn: function () {
    return Meteor.subscribe('singlePost', this.params._id);
  },
  
  onRun: function () {
    this.next();
  },
  onRerun: function () {
    this.next();
  },
  onBeforeAction: function () {
    this.next();
  },
  
  action: function () {
    this.render();
  }
});
